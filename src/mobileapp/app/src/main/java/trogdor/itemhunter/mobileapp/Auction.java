package trogdor.itemhunter.mobileapp;

import java.sql.Timestamp;

/**
 * A model for the current silent auction.
 */
public class Auction {
    private String name;
    private String orgName;
    private String url;
    private Timestamp startTime;
    private Timestamp endTime;

    public Auction(String name, String orgName, String url, Timestamp startTime, Timestamp endTime) {
        this.name = name;
        this.orgName = orgName;
        this.url = url;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Auction(String url, Timestamp startTime, Timestamp endTime) {
        this("", "", url, startTime, endTime);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }
}
