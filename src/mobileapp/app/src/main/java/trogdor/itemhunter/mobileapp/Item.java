package trogdor.itemhunter.mobileapp;

import java.sql.Timestamp;
import java.util.ArrayList;

/**
 * A model for items sold at silent auctions.
 */
public class Item {
    private String id;
    private String name;
    private String description;
    private String thumbnailUrl;
    private ArrayList<String> pictureUrls;

    private float currentBid;
    private int bidCount;
    private Timestamp bidStart;
    private Timestamp bidEnd;

    public Item(String id, String name, String description, String thumbnailUrl,
                ArrayList<String> pictureUrls, float currentBid, int bidCount, Timestamp bidStart,
                Timestamp bidEnd) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.thumbnailUrl = thumbnailUrl;
        this.pictureUrls = pictureUrls;

        this.currentBid = currentBid;
        this.bidCount = bidCount;
        this.bidStart = bidStart;
        this.bidEnd = bidEnd;
    }

    public Item(String id, String name, String thumbnailUrl, float currentBid, int bidCount,
                Timestamp bidStart, Timestamp bidEnd) {
        this(id, name, "", thumbnailUrl, new ArrayList<String>(), currentBid, bidCount, bidStart, bidEnd);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public ArrayList<String> getPictureUrls() {
        return pictureUrls;
    }

    public void setPictureUrls(ArrayList<String> pictureUrls) {
        this.pictureUrls = pictureUrls;
    }

    public float getCurrentBid() {
        return currentBid;
    }

    public void setCurrentBid(float currentBid) {
        this.currentBid = currentBid;
    }

    public int getBidCount() {
        return bidCount;
    }

    public void setBidCount(int bidCount) {
        this.bidCount = bidCount;
    }

    public Timestamp getBidStartTime() {
        return bidStart;
    }

    public void setBidStartTime(Timestamp bidStart) {
        this.bidStart = bidStart;
    }

    public Timestamp getBidEndTime() {
        return bidEnd;
    }

    public void setBidEndTime(Timestamp bidEnd) {
        this.bidEnd = bidEnd;
    }
}
