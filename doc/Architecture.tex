\documentclass[12pt,oneside,letterpaper]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% packages %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{longtable}
\usepackage{graphicx}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% global styles %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newenvironment{packed_enumerate}{
  \vspace{-7mm}
  \begin{enumerate}
    \setlength{\itemsep}{0pt}
    \setlength{\parskip}{0pt}
    \setlength{\parsep}{0pt}
}{
  \end{enumerate}
  \vspace{-8mm}
}

\pagestyle{headings}
\oddsidemargin 0.25in \textwidth     6.25in \topmargin     0.4in
\textheight    8.5in

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Title Page %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\title{
  \bfseries ItemHunter\\
  Software Architecture\\
  Version 1.1
}

\author {
  \large{Trogdor!!}\\
  \emph{Computer Science Department}\\
  \emph{California Polytechnic State University}\\
  \emph{San Luis Obispo, CA USA}\\
}

\date{\today}
\maketitle \thispagestyle{empty}

\pagebreak

%%%%%%%%%%%%%%%% Table of Contents, Revision History, and Credists %%%%%%%%%%%%%%%%%%

\tableofcontents

\addcontentsline{toc}{section}{Revision History}

\addcontentsline{toc}{section}{Credits}

\section*{Credits}
\begin{tabular}{|l|l|p{2.5in}|l|}
\hline
\textbf{Name}&\textbf{Date}&\textbf{Role}&\textbf{Version}\\
\hline
Christine Pham&November 12, 2014&&1.0\\
\hline
Sterling Tarng&November 12, 2014&&1.0\\
\hline
Josh Terrell&November 12, 2014&&1.0\\
\hline
Joel Wilcox&November 12, 2014&&1.0\\
\hline
\end{tabular}

\section*{Revision History}
\begin{tabular}{|l|l|p{2.5in}|l|}
\hline
\textbf{Name}&\textbf{Date}&\textbf{Reason for Changes}&\textbf{Version}\\
\hline
All Team&November 12, 2014&Write initial version&1.0\\
\hline
Josh and Joel&November 15, 2014&Add sections for Shared Bidder App and SMS Bidding&1.1\\
\hline
&&&\\
\hline
\end{tabular}

\newpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Section 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

This document describes the architecture and technologies used in our ItemHunter application including the Mobile Bidder App, Admin Console, and communications with our database. For more information, see the Trogdor Vision and Scope and SRS Documents.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Section 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Problem Description}

Our application strives to make silent auctions more convenient and efficient for all those involved. We have a mobile application for bidders to view and bid on items from their smartphones. We also have an web interface for auction organizers that allows them to get setup quickly and allows them to get useful data about their auction.
%TODO should we mention the non-mobile solutions here too (shared devices + sms)?

\subsection{Mobile App}
Our mobile application is supported on the Android Operating System. For users to be able to access ItemHunter, they must use an Android device to download and launch the application. 

\subsection{Shared Bidder App}
The shared bidder application is supported on the Android Operating System. Organizations must download the app onto thier tablets and configure the app before each auction, specifying which items can be bid on using each tablet.

\subsection{SMS Bidding}
If they do not have a smartphone or prefer to not use mobile apps, users can participate via SMS. Organizations provide a phone number or email for the texts to go to. Server-side code then parses the messages, performs bidding functions, and may return messages back to the bidder.

\subsection{Admin Console}
The admin console uses a combination of web technologies (HTML, CSS, JavaScript) and Apex on top of VisualForce pages. Salesforce accounts and orgs are needed for logging in and managing auctions.

\subsection{Database}
We use Salesforce's standard database offering for orgs to store all data. This primarily includes Org info, auction data, and items.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Section 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Solution}
\subsection{Overview}

For an auction to occur via ItemHunter, administrators must be able to create auctions and populate auctions with items to be auctioned. Administrators do so by interacting with the admin console which uses the SalesForce API to persist the auction data in the database. Then the bidders place bids on items during the auction using the any of the bidding components: the mobile app, shared app, or SMS app. These applications interact with the database through a few layers (including the SalesForce API) to persist bids in the database. Finally, when the auction has finished, volunteers and administrators use the cashier console to checkout bidders who have won items.

\subsection{Components}

The solution lies in the components of ItemHunter interacting as parts of a whole.\\

\begin{packed_enumerate}
  \item{Database} - store information about auctions including items, users, and images.
    \item{SalesForce API} - provide CRUD operations on the information stored in the database.
    \item{Mobile App} - the primary bidding application deployed on the android operating system.
    \item{Shared App} - a secondary bidding application deployed on android tablets. This application is provided as a means to allow bidders without the mobile app to participate in bidding.
    \item{SMS App} - another secondary bidding application which allows users without the mobile app to participate in auctions. This application runs on salesforce.
    \item{Admin Console} - the interface administrators use to perform CRUD operations on auctions for their organizations. (Eg. add an item to an auction).
    \item{Cashier Console} - a subset of the Admin Console which serves cashiers in quickly and accurately checking out winning bidders.
\end{packed_enumerate}

\subsubsection{Figure 3-1: Deployment Diagram}
\includegraphics[width=1.0\textwidth]{deployment.png}

\subsubsection{Figure 3-2: Admin Console Dialog Map}
\includegraphics[width=1.0\textwidth]{AdminDialogMap.png}


\subsubsection{Figure 3-3: Bidder App Dialog Map}
\includegraphics[width=1.0\textwidth]{dialog-map-bidder.png}

\subsubsection{Figure 3-4: Database Schema}
\includegraphics[width=1.0\textwidth]{ClassDiagram_DatabaseSchema.png}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Section A %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\appendix

\section{Other Figures}

\subsection{Figure A-1: Logon Sequence Diagram}
\includegraphics[width=1.0\textwidth]{Logon_Sequence_Diagram.png}

\subsection{Figure A-2: Mobile App Place bid Sequence Diagram}
\includegraphics[width=1.0\textwidth]{bid-on-item-sequence-diagram.png}

\subsection{Figure A-3: SMS Place Bid Sequence Diagram}
\includegraphics[width=1.0\textwidth]{bid-on-item-sms-sequence-diagram.png}

\subsection{Figure A-4: Auction State Diagram}
\includegraphics[width=1.0\textwidth]{auction-state-diagram.png}

\subsection{Figure A-5: ItemHunter Use Case Diagram}
\includegraphics[width=1.0\textwidth]{ItemHunterUseCaseDiagram.png}

\subsection{Figure A-6: Mobile App Place Bid Activity Diagram}
\includegraphics[width=1.0\textwidth]{ActivityDiagram_PlaceBid.png}

\subsection{Figure A-7: Admin Console Check Out Activity Diagram}
\begin{center}
\includegraphics[height=1.0\textwidth]{ActivityDiagram_CheckOut.png}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Section B %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Issues List}
\textit{Open Issues:}


\end{document}
